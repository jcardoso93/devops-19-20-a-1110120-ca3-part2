package com.greglturnquist.payroll;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EmployeeTest {

    /**
     * Tests for getFirstName
     */

    @Test
    @DisplayName ("Test get First Name")
    void testGetFirstName_Success() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");
        String expected= "Frodo";

        //Act
        String real = oneEmployee.getFirstName();

        //Assert
        assertEquals(expected, real);

    }


    /**
     * Tests for setFirstName
     */

    @Test
    @DisplayName ("Test valid Name input")
    void testSetFirstName_ValidInput() {
        //Arrange
        Employee oneEmployee = new Employee("Frod", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");
        String expected= "Frodo";

        //Act
        oneEmployee.setFirstName("Frodo");
        String real = oneEmployee.getFirstName();

        //Assert
        assertEquals(expected, real);

    }

    @Test
    @DisplayName ("Test Set First Name - null")
    void testSetFirstName_Null () {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");

        try {
            //Act
            oneEmployee.setFirstName(null);
            fail();
        }
        //Assert
        catch (IllegalArgumentException invalidFirstName) {
            assertEquals("The First Name can´t be null or empty!", invalidFirstName.getMessage());
        }

    }

    @Test
    @DisplayName ("Test Set First Name - Empty")
    void testSetFirstName_Empty () {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");

        try {
            //Act
            oneEmployee.setFirstName("");
            fail();
        }
        //Assert
        catch (IllegalArgumentException invalidFirstName) {
            assertEquals("The First Name can´t be null or empty!", invalidFirstName.getMessage());
        }

    }

    /**
     * Tests for getLastName
     */

    @Test
    @DisplayName ("Test Get Last Name")
    void getLastName() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");
        String expected= "Baggins";

        //Act
        String real = oneEmployee.getLastName();

        //Assert
        assertEquals(expected, real);

    }

    /**
     * Tests for setLastName
     */

    @Test
    @DisplayName ("Test valid last Name input")
    void testSetLastName_ValidInput() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Bag", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");
        String expected= "Baggins";

        //Act
        oneEmployee.setLastName("Baggins");
        String real = oneEmployee.getLastName();

        //Assert
        assertEquals(expected, real);

    }

    @Test
    @DisplayName ("Test Set Last Name - null")
    void testSetLastName_Null () {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");

        try {
            //Act
            oneEmployee.setLastName(null);
            fail();
        }
        //Assert
        catch (IllegalArgumentException invalidLastName) {
            assertEquals("The Last Name can´t be null or empty!", invalidLastName.getMessage());
        }

    }

    @Test
    @DisplayName ("Test Set Last Name - Empty")
    void testSetLastName_Empty () {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");

        try {
            //Act
            oneEmployee.setLastName("");
        }
        //Assert
        catch (IllegalArgumentException invalidLastName) {
            assertEquals("The Last Name can´t be null or empty!", invalidLastName.getMessage());
        }

    }

    /**
     * Tests for getDescription
     */

    @Test
    @DisplayName ("Test get Description")
    void getDescription() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");
        String expected= "ring bearer";

        //Act
        String real = oneEmployee.getDescription();

        //Assert
        assertEquals(expected, real);

    }

    /**
     * Tests for setDescription
     */

    @Test
    @DisplayName ("Test valid Description input")
    void testSetDescription_ValidInput() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Bag", "ring", "Marketing Coordinator", "frodo1@hotmail.com");
        String expected= "ring bearer";

        //Act
        oneEmployee.setDescription("ring bearer");
        String real = oneEmployee.getDescription();

        //Assert
        assertEquals(expected, real);

    }

    @Test
    @DisplayName ("Test Set Description - null")
    void testDescription_Null () {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");

        try {
            //Act
            oneEmployee.setDescription(null);
        }
        //Assert
        catch (IllegalArgumentException invalidDescription) {
            assertEquals("The Description can´t be null or empty!", invalidDescription.getMessage());
        }
    }

    @Test
    @DisplayName ("Test Set Description - Empty")
    void testDescription_Empty () {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");

        try {
            //Act
            oneEmployee.setDescription("");
        }
        //Assert
        catch (IllegalArgumentException invalidDescription) {
            assertEquals("The Description can´t be null or empty!", invalidDescription.getMessage());
        }

    }

    /**
     * Tests for getJobTitle
     */

    @Test
    @DisplayName ("Test get Job Title")
    void geJobTitle() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");
        String expected= "Marketing Coordinator";

        //Act
        String real = oneEmployee.getJobTitle();

        //Assert
        assertEquals(expected, real);

    }

    /**
     * Tests for setJobTitle
     */

    @Test
    @DisplayName ("Test valiJob Title input")
    void testSetJobTitle_ValidInput() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Bag", "ring", "Marketing Coordinator", "frodo1@hotmail.com");
        String expected= "miau";

        //Act
        oneEmployee.setJobTitle("miau");
        String real = oneEmployee.getJobTitle();

        //Assert
        assertEquals(expected, real);

    }

    @Test
    @DisplayName ("Test Set Job Title - null")
    void testSetJobTitle_Null () {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");

        try {
            //Act
            oneEmployee.setJobTitle(null);
        }
        //Assert
        catch (IllegalArgumentException invalidJobTile) {
            assertEquals("The Job Title can´t be null or empty!", invalidJobTile.getMessage());
        }
    }

    @Test
    @DisplayName ("Test Set Job Title - Empty")
    void testJobTitle_Empty () {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");

        try {
            //Act
            oneEmployee.setJobTitle("");
        }
        //Assert
        catch (IllegalArgumentException invalidJobTitle) {
            assertEquals("The Job Title can´t be null or empty!", invalidJobTitle.getMessage());
        }

    }

    /**
     * Tests for getEmail
     */

    @Test
    @DisplayName ("Test get email")
    void geEmail() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");
        String expected= "frodo1@hotmail.com";

        //Act
        String real = oneEmployee.getEmail();

        //Assert
        assertEquals(expected, real);

    }

    /**
     * Tests for setEmail
     */

    @Test
    @DisplayName ("Test valid email input")
    void testSetEmail_ValidInput() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Bag", "ring", "Marketing Coordinator", "frodo@hotmail.com");
        String expected= "frodo1@hotmail.com";

        //Act
        oneEmployee.setJobTitle("frodo1@hotmail.com");
        String real = oneEmployee.getJobTitle();

        //Assert
        assertEquals(expected, real);

    }


    @Test
    @DisplayName("Test Valid email input - numbers")
    public void validEmailInput_numbers() {

        //Arrange:
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");
        String expected = "1110203@gmail.com";

        //Act:
        oneEmployee.setEmail("1110203@gmail.com");
        String real = oneEmployee.getEmail();

        //Assert:
        assertEquals(expected, real);

    }


    @Test
    @DisplayName("Test Valid email input ")
    public void validEmailInput_1() {

        //Arrange:
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");
        String expected = "12345678@isep.ipp.pt";

        //Act:
        oneEmployee.setEmail("12345678@isep.ipp.pt");
        String real = oneEmployee.getEmail();

        //Assert:
        assertEquals(expected, real);

    }

    @Test
    @DisplayName("Test Valid email input ")
    public void validEmailInput_2() {

        //Arrange:
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");
        String expected = "joo_a3sd.12@live.com.au";

        //Act:
        oneEmployee.setEmail("joo_a3sd.12@live.com.au");
        String real = oneEmployee.getEmail();

        //Assert:
        assertEquals(expected, real);

    }


    @Test
    @DisplayName ("Test Set email - null")
    void testSetEmail_Null () {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");

        try {
            //Act
            oneEmployee.setEmail(null);
        }
        //Assert
        catch (IllegalArgumentException invalidEmail) {
            assertEquals("The email can´t be null or empty!", invalidEmail.getMessage());
        }
    }

    @Test
    @DisplayName ("Test Set email - Empty")
    void testSetEmail_Empty () {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");

        try {
            //Act
            oneEmployee.setEmail("");
        }
        //Assert
        catch (IllegalArgumentException invalidEmail) {
            assertEquals("The email can´t be null or empty!", invalidEmail.getMessage());
        }

    }


    @Test
    @DisplayName("Test Invalid email input - No @")
    public void invalidEmailInput() {

        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");

        //Act:
        try {
            oneEmployee.setEmail("emailgmail.com");
        }

        //Assert
        catch (IllegalArgumentException invalidEmail) {
            assertEquals("The email it´s not valid", invalidEmail.getMessage());
        }

    }

    /**
     * Tests  invalid email input - no dot
     */

    @Test
    @DisplayName("Test Invalid email input - No Dot")
    public void invalidEmailInput_noDot() {
        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");

        //Act:
        try {
            oneEmployee.setEmail("email@gmailcom");
        }

        //Assert
        catch (IllegalArgumentException invalidEmail) {
            assertEquals("The email it´s not valid", invalidEmail.getMessage());
        }

    }


    @Test
    @DisplayName("Test Invalid email input - Two Dots")
    public void invalidEmailInput_TwoDots() {

        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");

        //Act:
        try {
            oneEmployee.setEmail("email@gmail..com");
        }

        //Assert
        catch (IllegalArgumentException invalidEmail) {
            assertEquals("The email it´s not valid", invalidEmail.getMessage());
        }

    }

    @Test
    @DisplayName("Test Invalid email input - Two at sign")
    public void invalidEmailInput_TwoAtSign() {

        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");

        //Act:
        try {
            oneEmployee.setEmail("email@@gmail.pt");
        }

        //Assert
        catch (IllegalArgumentException invalidEmail) {
            assertEquals("The email it´s not valid", invalidEmail.getMessage());
        }

    }


    @Test
    @DisplayName("Test Invalid email input - Two at sign - Different Places")
    public void invalidEmailInput_TwoAtSign_differentPlaces() {

        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");

        //Act:
        try {
            oneEmployee.setEmail("email@123@gmail.pt");
        }

        //Assert
        catch (IllegalArgumentException invalidEmail) {
            assertEquals("The email it´s not valid", invalidEmail.getMessage());
        }

    }


    @Test
    @DisplayName("Test Invalid email input")
    public void invalidEmailInput_NoAtSign_NoDot() {

        //Arrange
        Employee oneEmployee = new Employee("Frodo", "Baggins", "ring bearer", "Marketing Coordinator", "frodo1@hotmail.com");

        //Act:
        try {
            oneEmployee.setEmail("emailgmailcom");
        }

        //Assert
        catch (IllegalArgumentException invalidEmail) {
            assertEquals("The email it´s not valid", invalidEmail.getMessage());
        }

    }






}