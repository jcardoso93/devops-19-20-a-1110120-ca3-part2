# **Class Assignment 2 - Part 2 - Build Tools with Gradle**

------  

## Relatório Técnico | Ferramentas de compilação com Gradle

### **Breve Descrição**

Este tutorial faz parte da segunda parte da "Class Assignment 2" e o seu objetivo é converter a versão básica (ou seja, pasta "basic") da aplicação Tutorial em Gradle (em vez de Maven).

### **Pré-Requisitos**

* Instalação do [Git](https://git-scm.com/downloads) e [configuração de um editor](https://git-scm.com/book/en/v2/Customizing-Git-Git-Configuration). É necessário possuir um pacote de linha de comandos git e um IDE que suporta o Git

* Neste tutorial, foi usado o Git no [Cmder](https://cmder.net/), um emulador portátil para o Windows e o [IntelliJ IDEA](https://www.jetbrains.com/idea/)

* Instalação do Java JDK 8

    * Pode ser usado o seguinte comando para verificar a versão do java instalada:

        ```
        $ java -version
        java version "1.8.0_121"
        ```

* Apache Log4J 2

* Instalar a ultima versão do [Gradle](https://gradle.org/install/) (Versão 6.3)

  * Depois de instalado é possível ver a versão do gradle e verificar se o mesmo está instalado usando o comando:

    ```
    $ gradle -version
    ```  

* Download do tutorial [tut-react-and-spring-data-rest](https://github.com/spring-guides/tut-react-and-spring-data-rest). Que, tendo seguido as "Class Assignments" anteriores, já deverá estar no repositório.


### **Guia Passo-a-Passo**

#### **Criar um novo Branch "tut-basic-gradle"**

Antes de começar é necessário criar um novo branch com o nome tut-basic-gradle. Para tal, pode ser usado o seguinte comando:

```sh
$ git checkout -b tut-basic-gradle
```

Depois, no diretório ***ca2***,  deve ser criada uma nova pasta com o nome ***part2***:

```sh
$mkdir part2
```

#### **Começar um novo projeto Spring com Gradle**

Em https://start.spring.io, deverá ser criado um novo projeto Spring com Gradle, com as seguintes dependências:

  * Rest Repositories;
  * Thymeleaf;
  * JPA;
  * H2

O site deverá gerar um arquivo zip com o projeto Gradle com Java e Spring Boot 2.2.6 e as dependências selecionadas (ver exemplo abaixo):

![SpringBoot.PNG](https://bitbucket.org/repo/6zrb449/images/1262134271-SpringBoot.PNG)

**Extrair o conteúdo do zip**

Depois, deverá ser extraído todo o conteúdo da pasta demo (do zip gerado) para a pasta ca2/part2/ do repositório. Agora, o repositório vai ter uma aplicação Spring "vazia" que pode ser construída usando o Gradle. É possivel verificar as tarefas disponíveis do Gradle executando o comando:

```sh
$gradlew tasks
```

**Apagar a pasta src**

A pasta src deve ser apagada, uma vez que será utilizado o código do "basic tutorial"

**Copiar todos os ficheiros src da pasta basic**

Agora deve ser copiada a pasta **src**, assim como todos os seus conteúdos, da pasta **basic** do ca1 para esta nova pasta (/ca2/part2/).

Também devem ser copiados os ficheiros **webpack.config.js** e **package.json**. Estes ficheiros são necessários para configurar a parte web (frontend) do projeto.

Depois, deve ser verificado se a pasta src/main/resources/static/ possui a pasta build, se tiver essa pasta deverá ser apagada, uma vez que a mesma será gerada posteriormente pelo javascript da ferramenta webpack.

#### **Experimentar a aplicação**

Agora é possível experimentar a aplicação usando o comando:

```sh
$gradlew bootRun
```

A webpage http://localhost:8080/ deverá estar vazia. Isto acontece porque falta adicionar ao gradle o plugin para lidar com o código frontend, que será feito de seguida.


#### **Adicionar o plugin "org.siouan.frontend" ao Gradle**

Agora, para que o Gradle consiga lidar com o frontend, é necessário adicionar o plugin [**org.siouan.frontend**](https://github.com/Siouan/frontend-gradle-plugin). Este plugin é muito parecido ao usado no projeto com Maven ([**frontend-maven-plugin**](https://github.com/eirslett/frontend-maven-plugin)).

**Adicionar o plugin ao build.gradle**

Para adicionar o plugin ao projeto é necessário, primeiro, adicionar a seguinte linha ao "bloco" de plugins no build.gradle:

```sh
id "org.siouan.frontend" version "1.4.1"
```    

No entanto, por si só esse passo não é suficiente, agora, é necessário adicionar o código abaixo ao build.gradle para con figurar o plugin anterior:

```sh
frontend {
  nodeVersion = "12.13.1"
  assembleScript = "run webpack"
}
```    

Por fim, é necessário atualizar a secção objeto scripts em package.json para configurar a execução do webpack (segunda linha dentro de "scripts"):

```sh
"scripts": {
  "watch": "webpack --watch -d",
  "webpack": "webpack"
},
```

A linha "*watch*" já deverá estar introduzida, pelo que apenas será necessário adicionar a linha "*webpack*".

#### **Fazer Build**

Se o comando ***gradlew build*** for agora executado, as tarefas relacionadas com o frontend também serão adicionadas e o código frontend é gerado:   

```sh
$gradlew build
```

#### **Experimentar a aplicação**

Agora, é possível executar a aplicação correndo o comando:

```sh
$gradlew bootRun
```

A webpage http://localhost:8080/ já não deverá estar vazia e é agora possível experimentar a aplicação.


**Fazer commit das alterações para o repositório**

Estando tudo a funcionar deve ser efetuado o commit para o repositório no branch criado anteriormente (tut-basic-gradle):

```sh
$git add .
$git commit -m "mensagem do commit"
$git push -u origin --all
```


#### **Adicionar uma tarefa para copiar o ficheiro .jar gerado para uma nova pasta "dist"**

Nesse passo será necessário adicionar uma nova task no Gradle para copiar o ficheiro .jar gerado para uma pasta chamada "dist", localizada ao nível da pasta raiz do projeto (/ca2/part2).

Antes de criar a task deve ser criada a pasta "dist" que receberá a copia do .jar gerado:

```sh
$mkdir dist
```

Depois, para adicionar esta task, deve ser introduzido no build.gradle o seguinte:

```sh
task copyJar (type: Copy) {
	group = "DevOps"
	description = "Copy the generated jar to a folder named dist"
	from 'build/libs/'
	into 'dist'
}
```

Resumo das propriedades e métodos utilizadas nesta task:

|    Propiedade/Método     |    Descrição                                                 |
|-------------------|---------------------------------------------------------------------|
|    type:Copy      |    Copia arquivos para um diretório de destino.                     |
|    group          |    O grupo de tarefas ao qual a task pertence.                      |
|    description    |    A descrição desta tarefa.                                        |
|    from           |    Especifica os arquivos ou diretórios de origem para uma cópia.   |
|    into           |    Especifica o diretório de destino para uma cópia.                |

Agora, correndo o comando ***gradlew build***, pode-se verificar se a aplicação está a compilar.


**Verificar se a task foi criada**

É possível verificar se a task foi criada executando o seguinte comando:

```sh
$gradlew tasks
```

A task deverá aparecer no grupo de tasks de devops.


**Executar a task**

Para executar a task e verificar se a mesma funciona, deve ser executado o seguinte comando:

```sh
$gradlew copyJar
```
Agora é possível verificar se o ficheiro .jar foi copiado para a pasta dist.


**Fazer commit das alterações para o repositório**

Estando tudo a funcionar deve ser efetuado o commit para o repositório no branch criado anteriormente (tut-basic-gradle):

```sh
$git add .
$git commit -m "mensagem do commit"
$git push -u origin --all
```


#### **Adicionar uma tarefa para apagar os ficheiros gerados pelo webpack**

Neste passo deverá ser adicionada uma nova tarefa para apagar os ficheiros gerados pelo webpack. Geralmente estes ficheiros estão localizados em "*src/main/resources/static/built/*". Esta nova tarefa deverá ser executada automaticamente pelo Gradle antes da task **clean**.

Para adicionar esta tarefa, a task abaixo terá de ser adicionada ao build.gradle:

```sh
task deleteWebpack (type: Delete) {
	group = "DevOps"
	description = "Deletes all the files generated by webpack"
	delete 'src/main/resources/static/built'
}
```

A task foi adicionada ao grupo de tasks de DevOps e é possivel verificar se foi criada utilizando o comando **gradlew tasks**.

Resumo das propriedades e métodos utilizadas nesta task:

|    Propiedade/Método    |    Descrição                                                       |
|-------------------------|--------------------------------------------------------------------|
|    type:Delete          |    Apaga ficheiros ou diretórios.                                  |
|    group                |    O grupo de tarefas ao qual a task pertence.                     |
|    description          |    A descrição desta tarefa.                                       |
|    delete               |    O conjunto de arquivos que serão eliminados por esta tarefa.    |


No entanto, para a task ser executada automaticamente antes da task clean é necessário adicionar a seguinte dependência à task clean:

```sh
clean.dependsOn deleteWebpack
```

O dependsOn vai criar uma dependência pelo que vai "dizer" que sempre que a task clean for executada deverá primeiro executar a task deleteWebpack.

Agora, ao ser executada a task clean, a task deleteWebpack adicionada anteriormente vai ser executada primeiro, apagando assim os ficheiros gerados pelo webpack. É possível testar o seu funcionamento executando a task clean:

```sh
$gradlew clean
```
Depois de executada a task é possível verificar que os conteúdos gerados pelo webpack foram apagados.




**Fazer commit das alterações para o repositório**

Estando tudo a funcionar deve ser efetuado o commit para o repositório no branch criado anteriormente (tut-basic-gradle):

```sh
$git add .
$git commit -m "mensagem do commit"
$git push -u origin --all
```

#### **Fazer merge do branch tut-basic-gradle com o master**

Estando tudo a funcionar, deverá agora ser feito o merge do branch tut-basic-gradle com o master. Para tal, podem ser usados os seguintes comandos:

```sh
$git checkout master
$git merge tut-basic-gradle
$git add .
$git push -u origin –all
```

Por fim, é necessário criar uma nova tag (ca2-part2): 

```sh
$ git tag -a ca2-part2 -m "Class Assignment 2 - Part 2"
$ git push -u origin --tag
```


### **Alternativa ao Gradle - Apache Ant - part 2**

O Ant, apesar de ser uma boa ferramenta de compilação, que fornece bastante controlo no processo de compilação, é bastante complicado para projetos mais complexos e de maior dimensão, como o caso desta segunda parte que tem muitas dependências. Por esse motivo ele não foi implementado nesta parte e apenas foi feita a sua implementação na primeira parte. No entanto, seguem alguns links que explicam como poderia ser criado um novo projeto Spring com o Ant: 

  * https://docs.spring.io/spring-boot/docs/current/reference/html/using-spring-boot.html#using-boot-ant.
  * https://docs.spring.io/spring-boot/docs/2.1.7.BUILD-SNAPSHOT/reference/html/build-tool-plugins-antlib.html


### **Gradle vs Ant**

O Gradle é uma poderosa ferramenta de compilação, relativamente recente e que tem variadas vantagens em relação a outras ferramentas, como o Ant, uma vez que não usa XML. Ao contrário do Ant e do Maven, que são baseados em XML, o DSL do Gradle é baseado em groovy e, por esse motivo, é possível codificar em vez de escrever/configurar usando XML.

Alguns pontos de destaque do Gradle são os seguintes:

  *	Fornece padronização enquanto permanece flexível
  *	Fácil de ler e escrever scripts de compilação
  *	Melhor para lidar com várias versões de dependências
  *	Capaz de lidar com várias linguagens e tecnologias de programação
  *	O Gradle DSL (Domain-Specific Language) torna a estrutura de configuração simples
  *	O Gradle fornece melhorias de desempenho usando cache de compilação e o Gradle Daemon
  *	Fácil integração do IDE

O Ant é uma ferramenta altamente configurável que é muito boa para controlar o processo de compilação. Sendo mesmo uma excelente ferramenta para quando é necessário o máximo de controlo possível durante o processo de criação.

Pontos de destaque do Apache Ant relativamente ao Gradle:

  *	Melhor controlo sobre o processo geral de compilação
  *	Flexível para trabalhar com qualquer processo de trabalho

Contudo, o Ant é já uma ferramenta “antiga” e que também poderá trazer algumas desvantagens em relação ao Gradle. Entre essas desvantagens destaca-se o facto de os arquivos de compilação serem baseados em XML, podendo tornar-se grandes e insustentáveis. Outra desvantagem é o facto de os scripts de compilação serem mais trabalhosos de escrever. Adicionalmente, é muito mais difícil conseguir a integração do IDE o que é uma considerável desvantagem do Ant em relação ao Gradle.

Entre as várias ferramentas de compilação, o Ant poderá ser útil para projetos mais pequenos, enquanto que o Gradle, é uma ferramenta mais recente e que oferece uma maior flexibilidade.

### **Sumário**

Este tutorial pertence à segunda parte da "Class Assignment 2" e forneceu instruções para:

  - Começar um novo projeto Spring co Gradle
  - Adicionar plugins ao Gradle
  - Compilar (build) uma aplicação usando o Gradle
  - Criar tarefas (tasks) no Gradle
  - Ver as tasks que estão criadas
  - Executar tasks do gradle na linha de comandos
  - Adicionar dependências ao Gradle para correr testes

Além disso, foi também apresentada uma solução tecnológica alternativa à ferramenta Gradle, embora não tenha sido implementada nesta segunda parte.

### **Conclusão**

As ferramentas de compilação (Build Tools) são programas que permitem automatizar a criação de aplicações executáveis a partir do código-fonte. Esta automação de compilação é conseguida através da criação de “scripts” e automatização de tarefas que os developers de software usam nas suas atividades diárias. Em projetos grandes o uso destas ferramentas é essencial, uma vez que permitem que o processo de criação de software seja mais consistente. 

Existem várias ferramentas de automatização de compilação: Contudo, entre as várias ferramentas de compilação existentes, o Gradle apresenta uma maior flexibilidade e facilidade de integração. Sendo uma boa opção para projetos de grandes dimensões e mais complexos.

### **Referências**

- Overview of Build Tools - DevOps Lecture 3a  
- Gradle - DevOps Lecture 3b
- https://gradle.org/
- https://ant.apache.org/
- https://spring.io/ 
